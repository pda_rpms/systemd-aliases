Name:		systemd-aliases
Summary:	Short aliases for systemctl and journalctl
Summary(ru_RU.UTF-8): Короткие псевдонимы для systemctl и journalctl
Version:	1.0.1
Release:	3%{?dist}
License:	Public Domain
URL:		https://gitlab.com/pda_rpms/systemd-aliases
Group:		System Environment/Shells
Packager:	Dmitriy Pomerantsev <pda2@yandex.ru>
Source0:	aliases_systemd.sh
Source1:	sc
Source2:	log
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-root
%{?systemd_requires}
Requires:	bash
BuildRequires: systemd

%description
The package provides short aliases (sc for systemctl and log for
journalctl), simplifying the systemd usage. Also, that aliases support
bash's autocompletion.

%description -l ru_RU.UTF-8
Пакет предоставляет короткие псевдонимы (sc для systemctl и log для
journalctl), упрощая использование systemd. Так же псевдонимы поддерживают
автодополнение в bash.

%build
# None required

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_sysconfdir}/profile.d
%{__install} -pm 0644 %{SOURCE0} %{buildroot}%{_sysconfdir}/profile.d/

%{__mkdir_p} %{buildroot}%{_sysconfdir}/bash_completion.d
%{__install} -pm 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/bash_completion.d/
%{__install} -pm 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/bash_completion.d/

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%{_sysconfdir}/profile.d/*
%{_sysconfdir}/bash_completion.d/*

%changelog
* Thu Nov 28 2019 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.0 - 3
- License changed for Public Domain.

* Sat Dec 02 2018 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.1 - 2
- No aliases in plain Bourne shell.

* Fri Nov 23 2018 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.1 - 1
- Sudo compatibility provided.

* Fri Nov 23 2018 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.0 - 1
- Initial package.
